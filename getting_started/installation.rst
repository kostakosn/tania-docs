Installation
============

This software is built with `Go
<https://golang.org/>`_ programming language. It means you will get an executable binary to run on your machine. You don’t need extra software like MAMP, XAMPP, or WAMP to run **Tania**, but you may need MySQL database if you choose to use it instead of SQLite (*the default database*.)

The easiest way to install Tania is using pre-built binaries for Windows (x64) and Linux (x64). You can download it from the `release page <https://github.com/Tanibox/tania-core/releases/tag/1.5.1>`_. After the download process is finished, you can unzip it, and run it from Windows Command Prompt/Powershell by using :file:`.\tania-core.exe` or from Linux terminal by using :file:`./tania-core`.

If your OS is neither Windows (x64) nor Linux (x64), then you must build Tania by yourself. You can follow this instruction below.

Prerequisites
-------------

- `Go <https://golang.org/>`_ 1.11.x
- `NodeJS <https://nodejs.org/en/>`_ 8 or 10

Building Instructions
---------------------

and so on and on....

