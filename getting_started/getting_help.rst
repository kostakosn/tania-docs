Getting Help
============

Raising Issues
--------------

Please raise any bug reports on `Tania’s Github issue trackers <https://github.com/Tanibox/tania-core/issues>`_. Be sure to search the list to see if your issue has already been raised.

A good bug report is one that make it easy to understand what you were trying to do and what went wrong.

Feature requests
----------------

For feature requests, please raise and discuss them on `Tania’s Telegram group <https://t.me/usetania>`_.