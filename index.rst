Tania Documentation
===================

.. toctree::
   :maxdepth: 2
   :caption: Table Of Contents
   :name: sec-getting_started
   
   getting_started/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Using Tania
   :name: sec-using_tania   
   
   using_tania/index.rst   

