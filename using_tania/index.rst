Using Tania
===========

.. toctree::
   :maxdepth: 1
   :name: using_tania
   
   initial_setup
   inventory
   dashboard
   area
   crop
   reservoir
   settings

